#include <stdio.h>
#include <limits.h>
int main () {
    printf ("Tipo \t\t Tamaño [Bytes]\t Minimo \t Maximo\n") ;
    // Variables Enteras
    printf ("char \t\t %lu \t\t\t\t\t %d \t %d \t\n", sizeof(char), CHAR_MIN, CHAR_MAX);  
    printf ("uchar \t\t %lu \t\t\t\t\t %d \t %d \t\n", sizeof(char), 0, UCHAR_MAX);  
    printf ("short \t\t %lu \t\t\t\t\t %d \t %d \t\n", sizeof(short), SHRT_MIN, SHRT_MAX);
    printf ("ushort \t\t %lu \t\t\t\t\t %d \t %d \t\n", sizeof(short), 0, USHRT_MAX);
    printf ("int \t\t %lu \t\t\t\t\t %d \t %d \t\n", sizeof(int), INT_MIN, INT_MAX);
    printf ("long \t\t %lu \t\t\t\t\t %d \t %d \t\n", sizeof(long), LONG_MIN, LONG_MAX);
    printf ("long long \t\t %lu \t\t\t\t\t %lld \t %lld \t\n", sizeof(long long),0, __LONG_LONG_MAX__);

    // Puntos Flotantes
     printf ("float \t\t %lu \t\t\t\t\t %e \t %e \t\n", sizeof(float), __FLT_MIN__, __FLT_MAX__);
     printf ("double \t\t %lu \t\t\t\t\t %d \t %d \t\n", sizeof(double), __DBL_MIN__, __DBL_MAX__);
     printf ("long double \t\t %lu \t\t\t\t\t %d \t %d \t\n", sizeof(long double), __LDBL_MIN__, __LDBL_MAX__);



}